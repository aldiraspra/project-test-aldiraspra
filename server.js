// server.js
const express = require('express');
const path = require('path');
const axios = require('axios');

const app = express();
const port = process.env.PORT || 3000;

// Proxy untuk mengatasi CORS
app.use('/api', async (req, res) => {
  try {
    const response = await axios.get(`https://suitmedia-backend.suitdev.com${req.url}`);
    res.json(response.data);
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
});

// Serve static files (untuk HTML, CSS, JS, dll.)
app.use(express.static(path.join(__dirname, 'public')));

// Jalankan server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
