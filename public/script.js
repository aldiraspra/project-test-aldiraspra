// script.js

// Fungsi untuk mengambil data dari API
// Fungsi untuk mengambil data dari API
async function fetchData() {
  const apiUrl = "https://suitmedia-backend.suitdev.com/api/ideas?page[number]=1&page[size]=10&append[]=small_image&append[]=medium_image&sort=-published_at";

  try {
    const response = await fetch(apiUrl, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
    });

    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const data = await response.json();

    // Lakukan sesuatu dengan data yang diterima, misalnya render ke halaman
    console.log(data);

    // Panggil fungsi atau logika lain yang membutuhkan data API
    handleApiData(data);
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}

//ajax
$.ajax({
  url: 'https://suitmedia-backend.suitdev.com/api/ideas',
  method: 'GET',
  data: {
      'page[number]': 1,
      'page[size]': 10,
      'append[]': ['small_image', 'medium_image'],
      'sort': '-published_at'
  },
  headers: {
      'Content-Type': 'application/json', // Tambahkan header ini
      'Accept': 'application/json'
  },
  success: function(response) {
      console.log(response);
  },
  error: function(error) {
      console.error(error);
  }
});


//START

document.addEventListener("DOMContentLoaded", function () {
  var elems = document.querySelectorAll(".parallax");
});

// scroll header
let lastScrollTop = 0;
let headerHeight = document.getElementById("main-header").offsetHeight;

window.addEventListener("scroll", () => {
  let scrollTop = window.scrollY;

  // Check the scroll direction
  if (scrollTop > lastScrollTop) {
    // Scrolling down, hide the header
    document.getElementById("main-header").style.top = `-${headerHeight}px`;
  } else {
    // Scrolling up, show the header with transparency
    document.getElementById("main-header").classList.add("transparent");
    document.getElementById("main-header").style.top = "0";
  }

  lastScrollTop = scrollTop;
});

window.addEventListener("scroll", () => {
  const parallaxImage = document.querySelector(".parallax img");
  const scrollPosition = window.pageYOffset;
  parallaxImage.style.transform = `translateY(${scrollPosition * 0.6}px)`; // Adjust speed as needed
});

const posts = [
  {
    id: 1,
    title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers",
    date: "2023-06-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 2,
    title:
      "Jangan Asal Pilih Influencer, Berikut cara Menyusun Strategi Influencer",
    date: "2023-05-01",
    thumbnail: "/assets/banner.jpg",
  },
  {
    id: 3,
    title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers",
    date: "2023-05-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 4,
    title:
      "Jangan Asal Pilih Influencer, Berikut cara Menyusun Strategi Influencer",
    date: "2023-05-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 5,
    title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers",
    date: "2023-07-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 6,
    title:
      "Jangan Asal Pilih Influencer, Berikut cara Menyusun Strategi Influencer",
    date: "2023-05-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 7,
    title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers",
    date: "2023-08-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 8,
    title:
      "Jangan Asal Pilih Influencer, Berikut cara Menyusun Strategi Influencer",
    date: "2023-05-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 9,
    title:
      "Jangan Asal Pilih Influencer, Berikut cara Menyusun Strategi Influencer",
    date: "2023-05-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 10,
    title:
      "Jangan Asal Pilih Influencer, Berikut cara Menyusun Strategi Influencer",
    date: "2023-05-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 11,
    title:
      "Jangan Asal Pilih Influencer, Berikut cara Menyusun Strategi Influencer",
    date: "2023-05-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 12,
    title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers",
    date: "2023-05-01",
    thumbnail: "/assets/post.png",
  },
  {
    id: 13,
    title: "Kenali Tingkatan Influencers berdasarkan Jumlah Followers",
    date: "2022-02-03",
    thumbnail: "/assets/post.png",
  },
];

let sortOption = "newest";
let postsPerPage = 10;

function addPostToDOM(post) {
  const postList = document.getElementById("postList");

  const postCard = document.createElement("div");
  postCard.classList.add("post-card");

  // Create a link element
  const postLink = document.createElement("a");
  postLink.href = `/post/${post.id}`; // Set the URL to navigate to

  const postImage = document.createElement("div");
  postImage.classList.add("post-image");

  const img = document.createElement("img");
  img.src = post.thumbnail;
  img.alt = "Post Image";

  img.loading = "lazy";
  postImage.appendChild(img);

  const postContent = document.createElement("div");
  postContent.classList.add("post-content");

  const postDate = document.createElement("div");
  postDate.classList.add("post-date");
  postDate.textContent = formatDate(post.date);
  postContent.appendChild(postDate);

  const postTitle = document.createElement("div");
  postTitle.classList.add("post-title");
  postTitle.textContent = post.title;
  postContent.appendChild(postTitle);

  postLink.appendChild(postImage);
  postLink.appendChild(postContent);

  postCard.appendChild(postLink); // Append the link instead of the post image and content

  postList.appendChild(postCard);
}

function formatDate(dateString) {
  const options = { year: "numeric", month: "long", day: "numeric" };
  const formattedDate = new Date(dateString).toLocaleDateString(
    "en-US",
    options
  );
  return formattedDate;
}

document.getElementById("sort").addEventListener("change", updateSort);

function sortPosts(posts) {
  const sortedPosts = [...posts];

  sortedPosts.sort((a, b) => {
    const dateA = new Date(a.date);
    const dateB = new Date(b.date);

    if (sortOption === "newest") {
      return dateB - dateA;
    } else if (sortOption === "oldest") {
      return dateA - dateB;
    } else {
      // Jika sortOption tidak sesuai, kembalikan array tanpa mengurutkan
      return 0;
    }
  });

  return sortedPosts;
}

function updateStatus(currentPage) {
  const statusContainer = document.getElementById("statusContainer");
  const totalPosts = posts.length;
  const startIndex = (currentPage - 1) * postsPerPage + 1;
  const endIndex = Math.min(startIndex + postsPerPage - 1, totalPosts);

  statusContainer.textContent = `Showing ${startIndex} - ${endIndex} of ${totalPosts}`;
}

function paginatePosts(posts, currentPage) {
  const startIndex = (currentPage - 1) * postsPerPage;
  const endIndex = startIndex + postsPerPage;
  return posts.slice(startIndex, endIndex);
}

document.addEventListener("DOMContentLoaded", function () {
  localStorage.setItem("sortOption", sortOption);
  localStorage.setItem("postsPerPage", postsPerPage);
  localStorage.setItem("currentPage", currentPage);

  const storedSortOption = localStorage.getItem("sortOption");
  const storedPostsPerPage = localStorage.getItem("postsPerPage");
  const storedCurrentPage = localStorage.getItem("currentPage");

  if (storedSortOption) {
    sortOption = storedSortOption;
  }
  if (storedPostsPerPage) {
    postsPerPage = parseInt(storedPostsPerPage);
  }
  if (storedCurrentPage) {
    currentPage = parseInt(storedCurrentPage);
  }
});

function renderPosts(currentPage) {
  const postList = document.getElementById("postList");
  postList.innerHTML = "";

  const sortedPosts = sortPosts(posts);
  const paginatedPosts = paginatePosts(sortedPosts, currentPage);

  paginatedPosts.forEach((post) => {
    addPostToDOM(post);
  });

  updateStatus(currentPage);
}

function updateSort() {
  sortOption = document.getElementById("sort").value;
  renderPosts(1);
}

function updatePerPage() {
  postsPerPage = parseInt(document.getElementById("showPerPage").value);
  renderPosts(1);
}

//nextpage
let currentPage = 1;

function prevPage() {
  if (currentPage > 1) {
    currentPage--;
    renderPosts(currentPage);
    updatePageStatus();
  }
  localStorage.setItem("currentPage", currentPage);
}

function nextPage() {
  const totalPage = Math.ceil(posts.length / postsPerPage);
  if (currentPage < totalPage) {
    currentPage++;
    renderPosts(currentPage);
    updatePageStatus();
  }
  localStorage.setItem("currentPage", currentPage);
}

function updatePageStatus() {
  document.getElementById("currentPage").textContent = currentPage;
}

renderPosts(currentPage); // Render posts with retrieved state

renderPosts(1);
